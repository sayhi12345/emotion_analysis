
# Python support can be specified down to the minor or micro version
# (e.g. 3.6 or 3.6.3).
# OS Support also exists for jessie & stretch (slim and full).
# See https://hub.docker.com/r/library/python/ for all supported Python
# tags from Docker Hub.
# FROM buildpack-deps:stretch

# If you prefer miniconda:
FROM continuumio/miniconda3

LABEL Name=emotion_analysis Version=0.0.1

RUN apt-get update && apt-get install -y \
 libpq-dev \
 build-essential \
 && rm -rf /var/lib/apt/lists/*

WORKDIR /app
ADD . /app

# Using pip:
RUN python3 -m pip install -r requirements.txt
ENTRYPOINT ["python3", "emotion_analysis.py"]

# Using pipenv:
#RUN python3 -m pip install pipenv
#RUN pipenv install --ignore-pipfile
#CMD ["pipenv", "run", "python3", "-m", "emotion_analysis"]

# Using miniconda (make sure to replace 'myenv' w/ your environment name):
# RUN conda app create -f environment.yml
# CMD /bin/bash -c "source activate myenv && python3 -m emotion_analysis"
