import os
import sched
import sys
import time
from datetime import date, datetime, timedelta

import numpy as np
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from pytz import timezone, utc
from snownlp import SnowNLP, sentiment


class Emotion():
    data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             'sentiment.marshal.4')
    sentiment.load(data_path)

    msg_count = 0
    emotion_val = 0.0
    happy_val = 0.0
    angry_val = 0.0
    yesterday = None
    today = None
    es = None
    live_id = None
    emotion_log = None

    def __init__(self, live_id):
        self.es = Elasticsearch([{
            'host': '23.248.163.18',
            'port': 9200
        }])
        self.msg_count = 0
        self.live_id = live_id
        self.emotion_val = 0.0
        self.happy_val = 0.0
        self.angry_val = 0.0
        self.today = self.get_timestamp()
        self.today_index = self.append_logstash_string(self.today)

    @staticmethod
    def get_timestamp():
        return datetime.now().strftime('20%y.%m.%d')

    @staticmethod
    def normalize_emotion_value(val):
        return float((val - 0.5) * 2)

    @staticmethod
    def append_logstash_string(today):
        return '30-logstash-' + today

    @staticmethod
    def remove_tag(message):
        if '@' in message:
            tag_index = message.find(' ')
            message = message[tag_index:]
        return message

    @property
    def get_emotion(self):
        return self.emotion_val

    @property
    def get_emotion_log(self):
        return self.emotion_log

    def query_emotion(self):
        try:
            s = Search(using=self.es, index=self.today_index) \
                .query('query_string', query='msg:msg AND c.live_id:%s*' % self.live_id)\
                .filter('range', time={"gte": 'now-30s/s', "lte": 'now/s', "format": "epoch_second"})
        except:
            print('es error!')

        s._params['size'] = 10000
        res = s.execute()
        msg_list = []
        self.emotion_decay(self.msg_count)
        self.msg_count = 0

        if res != None:
            for hit in res['hits']['hits']:
                try:
                    message = u'%s' % hit['_source']['c']['msg']
                    message = self.remove_tag(message)
                    msg_list.append(message)
                    self.msg_count += 1
                    # current = get_query_datetime(hit['_source']['c']['at'] / 1000)
                    emotion = sentiment.classify(message)
                    emotion = self.normalize_emotion_value(emotion)
                    self.emotion_val += emotion
                except KeyError:
                    print(hit['_source']['c'])

        self.show_emotion()

    def show_emotion(self):
        t = time.strftime('%H:%M:%S')
        self.emotion_log = t
        if self.emotion_val > 20:
            self.emotion_log += ',😍,%.3f' % self.emotion_val
        elif self.emotion_val > 15:
            self.emotion_log += ',😂,%.3f' % self.emotion_val
        elif self.emotion_val > 12:
            self.emotion_log += ',😆,%.3f' % self.emotion_val
        elif self.emotion_val > 8:
            self.emotion_log += ',😁,%.3f' % self.emotion_val
        elif self.emotion_val > 5:
            self.emotion_log += ',😊,%.3f' % self.emotion_val
        elif self.emotion_val > 2:
            self.emotion_log += ',😏, %.3f' % self.emotion_val
        elif self.emotion_val >= 0:
            self.emotion_log += ',😐, %.3f' % self.emotion_val
        elif self.emotion_val < -20:
            self.emotion_log += ',💀,%.3f' % self.emotion_val
        elif self.emotion_val < -15:
            self.emotion_log += ',😱,%.3f' % self.emotion_val
        elif self.emotion_val < -12:
            self.emotion_log += ',😡,%.3f' % self.emotion_val
        elif self.emotion_val < -8:
            self.emotion_log += ',😤,%.3f' % self.emotion_val
        elif self.emotion_val < -5:
            self.emotion_log += ',😠,%.3f' % self.emotion_val
        elif self.emotion_val < 0:
            self.emotion_log += ',😞,%.3f' % self.emotion_val

        print(self.emotion_log)
        with open('./log/%s_%s.csv' % (self.live_id, self.today), 'a') as f:
            f.write(self.emotion_log + '\n')

    def emotion_decay(self, msg_count):
        self.emotion_val *= 0.7


def run_scheduler(s, emotion, live_id):
    try:
        emotion.query_emotion()
    finally:
        # re-enter query emotion after 60 sec
        s.enter(30, 1, run_scheduler, (s, emotion, live_id,))


if __name__ == '__main__':
    if len(sys.argv) < 2:
        live_id = '2282757G'  # 館長: 2282757G 真亦: 2150422G
    else:
        live_id = sys.argv[1]

    print('Run scheduler...')
    emotion = Emotion(live_id)
    s = sched.scheduler(time.time, time.sleep)

    run_scheduler(s, emotion, live_id)
    try:
        s.run()
    except KeyboardInterrupt:
        print('Manual break by user')
