import os
import time
import sys
import sched
from datetime import date, datetime, timedelta

import numpy as np
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from pytz import timezone, utc
from snownlp import SnowNLP, sentiment
from emotion_analysis import Emotion

# data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
#                          'sentiment.marshal.4')
# sentiment.load(data_path)
# local_timezone = timezone('Asia/Shanghai')


def append_logstash_string(yesterday, today):
    return '30-logstash-' + yesterday, '30-logstash-' + today


def get_timestamp():
    '''
    return: yesterday, today
    '''
    yesterday = datetime.now() - timedelta(1)
    today = datetime.now()
    yesterday = yesterday.strftime('20%y.%m.%d')
    today = today.strftime('20%y.%m.%d')

    return yesterday, today


def get_query_datetime(at):
    return datetime.fromtimestamp(at)


def get_title(dic):
    title = list(dic.keys())[0]
    title = str(title.year) + '.' + str(title.month) + '.' + str(title.day)

    return title


def get_xticks(dic):
    keys = list(dic.keys())
    key_list = []
    for k in keys:
        # s_min = str(k.minute) if k.minute >= 10 else '0' + str(k.minute)
        # key_list.append(str(k.hour) + s_min)
        key_list.append(k.strftime('%H:%M'))

    return key_list

def show_emotion(live_id, today, t, emotion_value):
    emotion_log = t
    if emotion_value > 20:
        emotion_log += ',😆,%.3f' % emotion_value
    elif emotion_value > 12:
        emotion_log += ',😁,%.3f' % emotion_value
    elif emotion_value > 8:
        emotion_log += ',😊,%.3f' % emotion_value
    elif emotion_value > 2:
        emotion_log += ',😏, %.3f' % emotion_value
    elif emotion_value >= 0:
        emotion_log += ',😐, %.3f' % emotion_value
    elif emotion_value < -20:
        emotion_log += ',😈,%.3f' % emotion_value
    elif emotion_value < -12:
        emotion_log += ',😡,%.3f' % emotion_value
    elif emotion_value < -8:
        emotion_log += ',😠,%.3f' % emotion_value
    elif emotion_value < 0:
        emotion_log += ',😞,%.3f' % emotion_value

    print(emotion_log)
    with open('./log/%s_%s.csv' % (live_id, today), 'a') as f:
        f.write(emotion_log + '\n')


def write_msg_log(live_id, dt, messages):
    with open('./text/%s_%d.%d_messages.txt' % (live_id, dt.month, dt.day), 'a') as f:
        f.write(dt.strftime('20%y.%m.%d:%H:%M') + '\n')
        [f.write(msg + '\n') for msg in messages]
        f.write('----------\n')


def offline():
    # Get data from elk
    es = Elasticsearch([{
        'host': '23.248.163.18',
        'port': 9200
    }])

    msg = []
    allres = []
    # 館長:2282757G, 包膜:2149653G, 草包:2150439G, 阿寧:2114234G, 薰薰:2162175G, 川普:2128987G, 統神:2132991G, 真亦:2150422G
    live_id = '2282757G'
    yesterday, today = get_timestamp()
    yesterday_index, today_index = append_logstash_string(yesterday, today)

    # 2018.06.18
    s = Search(using=es, index=yesterday_index) \
        .query('query_string', query='msg:msg AND c.live_id:%s*' % live_id)\
        .filter('range', time={"gte": 'now-1d/d', "lte": 'now/d', "format": "epoch_second"})

    s2 = Search(using=es, index=today_index) \
        .query('query_string', query='msg:msg AND c.live_id:%s*' % live_id)\
        .filter('range', time={"gte": 'now-1d/d', "lte": 'now/d', "format": "epoch_second"})

    s._params['size'] = 10000  # max query size
    s2._params['size'] = 10000
    allres.append(s.execute())
    allres.append(s2.execute())
    emotion_dict = {}
    msg_count = []
    msg_list = []
    if s == None:
        first_item = allres[0]['hits']['hits'][0]
    else:
        first_item = allres[0]['hits']['hits'][1]
    t = get_query_datetime(first_item['_source']['c']['at'] / 1000)
    for res in allres:
        count, hit_count, emotion_value = (0, 0, 0.0)
        if res != None:
            for i, hit in enumerate(res['hits']['hits']):
                try:
                    message = u'%s' % hit['_source']['c']['msg']
                    current = get_query_datetime(
                        hit['_source']['c']['at'] / 1000)
                    emotion_value += Emotion.normalize_emotion_value(
                        sentiment.classify(message))
                    msg_list.append(message)
                    count += 1
                    if current - t >= timedelta(minutes=0.5):
                        if abs(emotion_value) > 10:
                            write_msg_log(live_id, current, msg_list)
                        show_emotion(live_id, today, t.strftime('%H:%M:%S'), emotion_value)
                        emotion_dict[t] = (emotion_value, count)
                        msg_list.clear()
                        count = 0
                        emotion_value *= 0.7  # emotion_value = 0
                        t = current
                    # print('text: ', hit['_source']['c']['msg'], ', prob:%.3f' % sentiment.classify(hit['_source']['c']['msg']))
                    hit_count += 1
                except KeyError:
                    print(hit['_source']['c'])

    # print(hit_count)
    lEmotion = []
    lMsgCount = []
    with open('./log/%s_emotion.csv' % live_id, 'a') as csv:
        csv.write('datetime,emotion,msg_count\n')
        for key, value in emotion_dict.items():
            emotion, count = value
            lEmotion.append(emotion)
            lMsgCount.append(count)
            csv.write('%s,%.3f,%d\n' % (key, emotion, count))

offline()