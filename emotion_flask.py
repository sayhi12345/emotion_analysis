import time
import threading
import sched
from flask import Flask, render_template, template_rendered

from emotion_analysis import Emotion


app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
live_ids = []
threads = []
live_id_key_emoji = {}


@app.route('/')
def index():
    return render_template('home.html', value='Please enter <url>/lid/<live_id>')


@app.route('/lid/<live_id>')
def home(live_id):
    global live_ids
    global live_id_key_emoji
    print('Live ID = %s' % live_id)
    if len(live_ids) < 1:
        live_ids.append(live_id)
        emotion = Emotion(live_id)
        live_id_key_emoji[live_id] = EmojiBackend(live_id, emotion)
        t = threading.Thread(target=live_id_key_emoji[live_id].exec_scheduler)
        t.start()
        threads.append(t)
        return render_template('home.html', value='No Request')
    elif live_id in live_ids:
        try:
            line = live_id_key_emoji[live_id].get_emoji.split(',')
            print(line[0])
        except:
            print('Exception')
        return render_template('home.html',
                               time=line[0],
                               emoji=line[1],
                               value=line[2])
    else:
        live_ids.append(live_id)
        emotion = Emotion(live_id)
        live_id_key_emoji[live_id] = EmojiBackend(live_id, emotion)
        t = threading.Thread(target=live_id_key_emoji[live_id].exec_scheduler)
        t.start()
        threads.append(t)
        return render_template('home.html', value='Wait...')

class EmojiBackend():
    live_id = None
    emotion = None
    emotion_log = None

    def __init__(self, live_id, emotion):
        self.live_id = live_id
        self.emotion = emotion
        print('Init Emoji: %s' % self.live_id)

    @property
    def get_emoji(self):
        return self.emotion_log

    def emoji_scheduler(self, s):
        try:
            print('Scheduler')
            self.emotion.query_emotion()
            self.emotion_log = self.emotion.get_emotion_log
        finally:
            # re-enter query emotion after 60 sec
            s.enter(30, 1, self.emoji_scheduler, (s,))

    def exec_scheduler(self):
        s = sched.scheduler(time.time, time.sleep)
        self.emoji_scheduler(s)
        try:
            s.run()
        except KeyboardInterrupt:
            print('Manual break by user')


if __name__ == '__main__':
    app.run(debug=True, port=8030)

