from snownlp import sentiment
import time
import os

path = os.listdir()

time_start = time.time()
print('training start')
sentiment.train('/Users/shane/Desktop/PythonSamples/snownlp/snownlp/sentiment/neg.txt', '/Users/shane/Desktop/PythonSamples/snownlp/snownlp/sentiment/pos.txt')
sentiment.save('sentiment.marshal.4')
print('train time: {%f}' % (time.time() - time_start))